%%%%%%%%%%%%%%%%%%%%%%% file template.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is a general template file for the LaTeX package SVJour3
% for Springer journals.          Springer Heidelberg 2010/09/16
%
% Copy it to a new file with a new name and use it as the basis
% for your article. Delete % signs as needed.
%
% This template includes a few options for different layouts and
% content for various journals. Please consult a previous issue of
% your journal as needed.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% First comes an example EPS file -- just ignore it and
% proceed on the \documentclass line
% your LaTeX will extract the file if required
\RequirePackage{fix-cm}
%
%\documentclass{svjour3}                     % onecolumn (standard format)
%\documentclass[smallcondensed]{svjour3}     % onecolumn (ditto)
\documentclass[smallextended]{svjour3}       % onecolumn (second format)
%\documentclass[twocolumn]{svjour3}          % twocolumn
%
\smartqed  % flush right qed marks, e.g. at end of proof
%
\usepackage{graphicx}
%
% \usepackage{mathptmx}      % use Times fonts if available on your TeX system
%
% insert here the call for the packages your document requires

\usepackage[separate-uncertainty=true,multi-part-units=single]{siunitx}
\usepackage[version=3]{mhchem}
\usepackage[printonlyused,nolist]{acronym}
\usepackage{booktabs}
\usepackage[sort,round,authoryear]{natbib}
\usepackage[hidelinks]{hyperref} %[hidelinks]

%\usepackage{latexsym}
% etc.
%
% please place your own definitions here and don't use \def but

%%%%%%%%%%%%%% Custom definitions %%%%%%%%%%%%%%

% Abbreviated number ranges

\newcommand{\nr}[2]{\numrange[range-phrase = --]{#1}{#2}}
\newcommand{\dr}[2]{\numrange[range-phrase = --]{#1}{#2} \si{\centi\metre}}

% Abbreviated units

\newcommand{\cm}[1]{\SI{#1}{\centi\metre}}
\newcommand{\mm}[1]{\SI{#1}{\milli\metre}}
\newcommand{\kg}[1]{\SI{#1}{\kilo\gram}}
\newcommand{\ha}[1]{\SI{#1}{\hectare}}
\newcommand{\gpcmc}[1]{\SI{#1}{\gram\per\centi\metre\cubed}}
\newcommand{\mgpcmc}[1]{\SI{#1}{\milli\gram\per\centi\metre\cubed}}


% Sub/Superscripts

\newcommand{\superscript}[1]{\ensuremath{^{\textrm{#1}}}}
\newcommand{\subscript}[1]{\ensuremath{_{\textrm{#1}}}}

%
% Insert the name of "your journal" with
\journalname{Geoderma}
%

\begin{document}

\begin{acronym}[UML]
\acro{BD}[$\rho_b$]{soil bulk density}
\acro{ANOVA}{analysis of variance}
\acro{DBH}[D\subscript{bh}]{diameter at breast height}
\acro{DOC}{dissolvable organic carbon fraction}
\acro{ESM}{equivalent soil mass}
\acro{FRLD}{fine root length density}
\acro{FRMD}{fine root mass density}
\acro{FW}{farm woodland}
\acro{MW}{mature grazed woodland}
\acro{OCC}[C\%]{organic carbon content}
\acro{PA}{pasture}
\acro{POM}{particulate organic matter}
\acro{rSOC}{chemically resistant soil organic carbon}
\acro{S+A}{sand and stable aggregates}
\acro{s+c}{silt and clay fraction}
\acro{SOC}{soil organic carbon}
\acro{SMC}[$\theta$]{mean soil moisture content}
\acro{vSMC}[$\theta_v$]{mean volumetric soil moisture content}
\acro{gSMC}[$\theta_g$]{mean gravimetric soil moisture content}
\acro{SP}{silvopasture}
\acro{WCC}{Woodland Carbon Code}
\acro{WRB}{World Reference Base}
\acro{CV}{coefficient of variation}
\acro{s+c-rSOC}{silt and clay minus the chemically resistant soil organic carbon}
\end{acronym}

<<load_packages,eval=TRUE,echo=FALSE,include=FALSE,message=FALSE,warning=FALSE>>=
library(knitr)

knit_hooks$set(plot.mar = function(before, options, envir) {
    if (before) par(
         mfrow = c( 1, 1),
         mar = c( 4.1, 3.9, 0.5, 0.5),
         mgp = c( 1.95, 0.6, 0),
         lend = "square",
         lwd = 0.5,
         bty="l"
         ) 
})

opts_chunk$set(
  cache = FALSE,
  echo = FALSE,
  include = FALSE,
  warning = FALSE,
  message = FALSE,
  fig.path = "",
  dev = "pdf"
  )

library(plyr)
library(dplyr)
library(magrittr)
#library(agricolae)
#library(reshape)
#library(tidyr)
#library(lubridate)
#library(ggplot2)
#library(RColorBrewer)

source('R/anova_extract.R')
source('R/comp_data.R')
source('R/DP.R')
source('R/SE.R')
source('R/sig.R')
source('R/sround.R')
source('R/se_lines.R')
source('R/ci95.R')

ash_mens <- tbl_df(
  read.table(
    "data/ash_mens.csv", 
    header = T, 
    sep = ","
  ) 
) 

load("data/ra_a_dbh_abg_carbon.RData")

@

\section*{\emph{Supplementary material}}

\subsubsection*{Total biomass}

<<CP_biomass_t.test,eval=TRUE,include=FALSE,cache=TRUE>>=


CP_biomass <- ash_mens %>%
  dplyr::filter(site == "CP") %>%
  dplyr::mutate(
    #kg with correction factor
    abgC = (
      10 ^ coef(ra_a_dbh_abg_carbon)[1] * dbh ^ coef(ra_a_dbh_abg_carbon)[2] * 1.02
    ),
    # mean carbon content (probably needs to be weighted mean)
    bgC = (0.000149 * (dbh / 10) ^ 2.12) * 1000 * 0.4827425,
    #kg
    totC = abgC + bgC,
    density = round(10000 / as.numeric(as.character(spacing)) ^ 2),
    totCha = (totC * density) / 1000
  ) %>%
  dplyr::select(treat,dbh,spacing,abgC,bgC,totC,totCha)

CP_totCha_t.test <- t.test(
  CP_biomass %>%
    dplyr::filter(treat == "SP", spacing == 2.0) %$% totCha,
  CP_biomass %>% dplyr::filter(treat == "FW", spacing == 2.5) %$% totCha
)

CP_abgC_t.test <- t.test(
  CP_biomass %>% dplyr::filter(treat == "SP", spacing == 2.0) %$% abgC,
  CP_biomass %>% dplyr::filter(treat == "FW", spacing == 2.5) %$% abgC
)

CP_bgC_t.test <- t.test(
  CP_biomass %>% dplyr::filter(treat == "SP", spacing == 2.0) %$% bgC,
  CP_biomass %>% dplyr::filter(treat == "FW", spacing == 2.5) %$% bgC
)


@

<<biomass,echo=FALSE,include=FALSE>>=

# From /classicthesis1/data/synthesis/ra_a_dbh_abg_carbon.RData
# model for aboveground biomass from dbh

# Tree biomass

smean <- function(x)
  sprintf("%.1f",mean(x))
sse <- function(x)
  sprintf("%.1f",se(x))

CP_biomass_c <- ash_mens %>%
  dplyr::filter(site == "CP",
                spacing %in% c(2.0,2.5)) %>%
  dplyr::mutate(
    # kg with correction factor
    abgC = (
      10 ^ coef(ra_a_dbh_abg_carbon)[1] *
        dbh ^ coef(ra_a_dbh_abg_carbon)[2] * 1.02
    ),
    # mean carbon content (probably needs to be weighted mean)
    bgC = (0.000149 * (dbh / 10) ^ 2.12) * 1000 * 0.4827425,
    # kg
    totC = abgC + bgC,
    density = round(10000 / as.numeric(as.character(spacing)) ^ 2),
    totCha = (totC * density) / 1000
  ) %>%
  dplyr::select(treat,dbh,spacing,abgC,bgC,totC,totCha) %>%
  dplyr::group_by(treat,spacing) %>%
  dplyr::summarise(
    dbh = mean(dbh),
    abgC_mean = smean(abgC),
    abgC_se = sse(abgC),
    bgC_mean = smean(bgC),
    bgC_se = sse(bgC),
    totC_mean = smean(totCha),
    totC_se = sse(totCha),
    totC_var = round(var(totCha)),
    length = n()
  )

# Create lists for use in tables:

SP_bmass <- as.list(CP_biomass_c[CP_biomass_c$treat == "SP",])
FW_bmass <- as.list(CP_biomass_c[CP_biomass_c$treat == "FW",])

@

The trees at Clapham Park are a native broadleaf mix \citep{Burgess2000}, comprising predominantly oak (\textit{Quercus robur}), ash (\textit{Fraxinus excelsior}), hornbeam (\textit{Carpinus betulus}), small-leaved lime (\textit{Tilia cordata}), and Field maple (\textit{Acer campestre}). In the absence of measurements of the other species, it has been assumed that the ash trees are representative of the other species also\footnote{Alternatively one could consider the results as a `what if' scenario, assuming only ash had been planted.}.

In order to calculate total biomass \ce{C} storage at the Clapham Park site, an allometric equation for total aboveground \ce{C} content developed from 42 destructive harvests\footnote{This data will be presented in a subsequent publication.} (Equation \ref{eq:power_rel1}) was applied to each of the \ac{DBH} (\mm{}) measurements taken in the \ac{FW}\footnote{Note that only trees planted within areas with a nominal spacing of \m{2.5} were used for the \acl{FW} treatment.} and \ac{SP} treatments giving aboveground \ce{C} (\kg{}), 

\begin{equation}\label{eq:power_rel1}
C_{abg} = \alpha D_{bh}^{\beta} CF
\end{equation}

\begin{description}
\setlength{\itemsep}{0cm}%
\setlength{\parskip}{0cm}%
\item[$\alpha$]{= regression coefficient (\Sexpr{sprintf("%.3f",coef(ra_a_dbh_abg_carbon)[1])})}
\item[$\beta$]{= regression coefficient (\Sexpr{sprintf("%.3f",coef(ra_a_dbh_abg_carbon)[2])})}
\item[$D_{bh}$]{= diameter at breast height (\mm{})}
\item[$CF$]{= correction factor (1.02) as defined by \citet{Baskerville1972}.}
\end{description}

<<>>=

ref1 <- sprintf("%.3f",mean(c(48.853,48.636,47.825,47.783))/100)

@

Belowground biomass was calculated by applying a second allometric equation for broadleaved trees \citep[p.51]{Jenkins2011} of the same format as Equation \ref{eq:power_rel1} using $\alpha = -3.83$ and $\beta = 2.12$ and \ac{DBH} in units of \cm{}. Values above and belowground biomass. This equation yields belowground root dry mass in metric tonnes, and therefore was multiplied by the mean \ce{C} content for ash trees of \Sexpr{ref1} to yield the average root \ce{C} content (metric tonnes).

% latex table generated in R 3.1.1 by xtable 1.7-3 package
% Wed Oct 22 17:32:51 2014
\begin{table}[bth]
\centering
\caption[Biomass \ce{C} at Clapham Park]{Estimated biomass \ce{C} at Clapham Park. Except where indicated, values are given in \tonne\ C \reciprocal\hectare. Note that part of the difference between \ce{C} totals is accounted for by the increased planting density in the \ac{SP} plots: trees were planted with \m{2 x 2} spacing, as opposed to \m{2.5 x 2.5} spacing in the \ac{FW} treatment.} 
\label{tab:clapham_final_biomass}
\begin{tabular}{lrllllllr}
  \toprule
  &  & \multicolumn{2}{c}{Aboveground} & \multicolumn{2}{c}{Belowground} & \multicolumn{3}{c}{Total \ce{C}} \\
     & \acs{DBH} & \multicolumn{2}{c}{(\si{\kilo\gram\ tree^{-1}})} & \multicolumn{2}{c}{(\si{\kilo\gram\ tree^{-1}})} & \multicolumn{3}{c}{(\mgpha{} of trees)} \\\cline{3-9}
      Treat &  (mm) & Mean & SE & Mean & SE & Mean & SE & $n$ \\
 \midrule
FW & 92 & 14.09 & 0.65 & 8.35 & 0.38 & 35.90 & 1.64 & 74 \\ 
SP & 121 & 24.98 & 1.17 & 14.78 & 0.69 & 99.41 & 4.65 & 74 \\ 
   \bottomrule
\end{tabular}
\end{table}

<<>>=
ref1 <- sround(CP_totCha_t.test$statistic)
ref2 <- sround(CP_totCha_t.test$parameter)
@

On a per-hectare basis, the \ac{SP} blocks were found to contain significantly more biomass \ce{C} than the \ac{FW} treatment (Welch two-sample $t$-test: $t= \Sexpr{ref1}$, $p<0.001$, $df=\Sexpr{ref2}$), despite containing 20 \% fewer trees. This is due to the fact that trees in the \ac{SP} blocks tended to have a greater \ac{DBH} (though were probably less tall) than trees in the \ac{FW} treatment, and consequently greater above and belowground biomass according to the allometric relationships.

\bibliographystyle{spbasic}      % basic style, author-year citations
%\bibliographystyle{spmpsci}      % mathematics and physical sciences
%bibliographystyle{spphys}       % APS-like style for physics
\bibliography{clapham_paper} 

\end{document}
% end of file template.tex
