se_lines <- function(
     x, 
     SE, 
     labels = "", 
     y, 
     bar_width = 0.1, 
     letters = FALSE,
     letter_dist = 0.1, 
     text_cex = 0.8,
     horiz = FALSE
     ) {
     
     
     if (horiz == TRUE) {
     
     for (i in 1:length(x)) {
          lines(
               c(x[i]+SE[i],x[i]-SE[i]),
               c(y[i],y[i])
          )
          
          lines(
               c(x[i]+SE[i],x[i]+SE[i]),
               c(y[i]-bar_width,y[i]+bar_width)
          )
          
          lines(
               c(x[i]-SE[i],x[i]-SE[i]),
               c(y[i]-bar_width,y[i]+bar_width)
          )
          
          # add significance
          if (letters == "n") next else {
          letter_location <- if (letters == "end") {
               x[i] + letter_dist
               } else if (letters == "base") {
                    letter_dist
                    }
          }
          
          text(
               x = letter_location,
               y = y[i],
               #x = 0.1,
               labels = labels[i],
               cex = text_cex
               #pos = 1
          )
          
     }
     
} else 
     if (horiz == FALSE) {
          
          for (i in 1:length(x)) {
               lines(
                    c(y[i],y[i]),
                    c(x[i]+SE[i],x[i]-SE[i])
               )
               
               lines(
                    c(y[i]-bar_width,y[i]+bar_width),
                    c(x[i]+SE[i],x[i]+SE[i])
               )
               
               lines(
                    c(y[i]-bar_width,y[i]+bar_width),
                    c(x[i]-SE[i],x[i]-SE[i])
               )
               
               # add significance
               if (letters == "n") next else {
                    letter_location <- if (letters == "end") {
                         x[i] + letter_dist
                    } else if (letters == "base") {
                         letter_dist
                    }
               }
               
               text(
                    y = letter_location,
                    x = y[i],
                    #x = 0.1,
                    labels = labels[i],
                    cex = text_cex
                    #pos = 1
               )
               
          }
          
     }
     
}